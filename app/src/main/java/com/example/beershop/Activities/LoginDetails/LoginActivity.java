package com.example.beershop.Activities.LoginDetails;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.example.beershop.Activities.UserSide.CustomerLandingActivity;
import com.example.beershop.Activities.Utilities.Utilities;
import com.example.beershop.MainActivity;
import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;

public class LoginActivity extends AppCompatActivity {
    private EditText user_email, user_pass;
    private MaterialButton btn_login;
    private TextView reset_pass, signup;
    private String email = "", password = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
    }

    private void init() {
        user_email = findViewById(R.id.user_email);
        user_pass = findViewById(R.id.user_pass);
        btn_login = findViewById(R.id.btn_login);
        reset_pass = findViewById(R.id.reset_pass);
        signup = findViewById(R.id.signup);

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = user_email.getText().toString().trim();
                String password = user_pass.getText().toString().trim();

                String logging_in_as = Utilities.getString(LoginActivity.this,"loginAs");

//                if (!email.equals("")){
//                    if (Patterns.EMAIL_ADDRESS.matcher(email).matches()){
//                        if(!password.equals("")){
//                            Toast.makeText(LoginActivity.this, "email"+email, Toast.LENGTH_SHORT).show();
//                        }else {
//                            user_pass.setError("Password Required");
//                        }
//                    }else {
//                        user_email.setError("Invalid Email");
//                    }
//                }else {
//                    user_email.setError("Email Required");
//                }

                if (!logging_in_as.isEmpty()){
                    if (logging_in_as.equals("admin")){
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }else {
                        Intent intent = new Intent(LoginActivity.this, CustomerLandingActivity.class);
                        startActivity(intent);
                        finish();
                    }
                }

            }
        });

        reset_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
                startActivity(intent);
            }
        });
    }
}
