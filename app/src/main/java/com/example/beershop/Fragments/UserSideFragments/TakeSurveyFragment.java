package com.example.beershop.Fragments.UserSideFragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;

public class TakeSurveyFragment extends Fragment {
    public TakeSurveyFragment() {
        // Required empty public constructor
    }
    private View v;
    private CardView survey1, survey2, survey3;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_take_survey, container, false);
        init();
        return v;
    }

    private void init() {
        survey1 = v.findViewById(R.id.survey1);
        survey2 = v.findViewById(R.id.survey2);
        survey3 = v.findViewById(R.id.survey3);

        survey1.setOnClickListener(Navigation.
                createNavigateOnClickListener(R.id.action_takeSurveyFragment_to_tierOneFragment));
//        survey2.setOnClickListener(Navigation.
//                createNavigateOnClickListener(R.id.action_mainCustomerFragment_to_takeSurveyFragment));
        survey3.setOnClickListener(Navigation.
                createNavigateOnClickListener(R.id.action_takeSurveyFragment_to_tierThreeFragment));
    }
}
