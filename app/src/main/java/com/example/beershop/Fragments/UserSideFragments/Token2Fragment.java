package com.example.beershop.Fragments.UserSideFragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.beershop.BuildConfig;
import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;

public class Token2Fragment extends Fragment {
    public Token2Fragment() {
        // Required empty public constructor
    }

    private View v;
    private MaterialButton yes, no;
    private ImageView qr_code, share_ic;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_token2, container, false);

        init();
        return v;
    }

    private void init() {
        no = v.findViewById(R.id.no);
        yes = v.findViewById(R.id.yes);
        qr_code = v.findViewById(R.id.qr_code);
        share_ic = v.findViewById(R.id.share_ic);

        yes.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_token2Fragment_to_tierThreeFragment));

        no.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.action_token2Fragment_to_mainCustomerFragment));

        share_ic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShareMe();
            }
        });
    }


    private void ShareMe() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        String shareSub = "Bet4Beers";
        String shareBody = "Hey check out my app at: https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID;
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, shareSub);
        sendIntent.putExtra(Intent.EXTRA_TEXT, shareBody);


        Intent shareIntent = Intent.createChooser(sendIntent, "Share using");
        startActivity(shareIntent);
    }
}
