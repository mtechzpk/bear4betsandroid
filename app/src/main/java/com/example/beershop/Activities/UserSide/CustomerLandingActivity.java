package com.example.beershop.Activities.UserSide;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.beershop.MainActivity;
import com.example.beershop.R;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.navigation.NavigationView;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

public class CustomerLandingActivity extends AppCompatActivity {
    public NavController navController;
    ImageView ivDrawer, ivNotification;
    private DrawerLayout drawer;
    NavigationView navigationView;
    RelativeLayout rlToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_landing);

        drawer = findViewById(R.id.user_drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        ivDrawer = findViewById(R.id.menu_ic);
        rlToolbar = findViewById(R.id.rvToolbar);
        setColor();

        initNavigation();
    }

    private void initNavigation() {
        ivDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer.openDrawer(GravityCompat.START, true);
            }
        });
        navController = Navigation.findNavController(this, R.id.nav_user_host_fragment);
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupWithNavController(navigationView, navController);
        navController.addOnDestinationChangedListener(new NavController.OnDestinationChangedListener() {
            @Override
            public void onDestinationChanged(@NonNull NavController controller, @NonNull NavDestination destination, @Nullable Bundle arguments) {
                if (destination.getLabel() != null) {
                    rlToolbar.setVisibility(View.VISIBLE);
//                    tvTitle.setText(destination.getLabel());
                }

            }
        });
    }

    public void setColor() {

        Menu menu = navigationView.getMenu();
        MenuItem tools1 = menu.findItem(R.id.mainCustomerFragment);
        SpannableString s1 = new SpannableString(tools1.getTitle());
        s1.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s1.length(), 0);
        tools1.setTitle(s1);

        MenuItem tools2 = menu.findItem(R.id.userProfileFragment);
        SpannableString s2 = new SpannableString(tools2.getTitle());
        s2.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s2.length(), 0);
        tools2.setTitle(s2);

        MenuItem menuItem = menu.findItem(R.id.creditsReceivedFragment);
        SpannableString s7 = new SpannableString(menuItem.getTitle());
        s7.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s7.length(), 0);
        menuItem.setTitle(s7);

        MenuItem menuItem1 = menu.findItem(R.id.creditsRemainingFragment);
        SpannableString s5 = new SpannableString(menuItem1.getTitle());
        s5.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s5.length(), 0);
        menuItem1.setTitle(s5);

        MenuItem menuItem2 = menu.findItem(R.id.orderHistoryFragment);
        SpannableString s6 = new SpannableString(menuItem2.getTitle());
        s6.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s6.length(), 0);
        menuItem2.setTitle(s6);

//        Menu menu3 = navigationView.getMenu();
//        MenuItem tools3= menu3.findItem(R.id.nav_rate_us);
//        SpannableString s3 = new SpannableString(tools3.getTitle());
//        s3.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s3.length(), 0);
//        tools3.setTitle(s3);

        MenuItem tools4 = menu.findItem(R.id.logout);
        SpannableString s4 = new SpannableString(tools4.getTitle());
        s4.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s4.length(), 0);
        tools4.setTitle(s4);

        MenuItem itmemenu = menu.findItem(R.id.refferals);
        SpannableString sReferrals = new SpannableString(itmemenu.getTitle());
        sReferrals.setSpan(new TextAppearanceSpan(this,R.style.TextAppearance44),0,sReferrals.length(),0);
        itmemenu.setTitle(sReferrals);

    }

    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.nav_menu, menu);

        // change color for icon 0
        Drawable yourdrawable = menu.getItem(0).getIcon(); // change 0 with 1,2 ...
        yourdrawable.mutate();
        yourdrawable.setColorFilter(getResources().getColor(R.color.white), PorterDuff.Mode.SRC_IN);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawers();
        } else if (!navController.getCurrentDestination().getLabel().toString().equals("fragment_main_customer")) {
            super.onBackPressed();
        } else {
            showCustomDialog1();
        }
    }

    private void showCustomDialog1() {
        final PrettyDialog pDialog = new PrettyDialog(CustomerLandingActivity.this);
        pDialog
                .setTitle("Exit")
                .setMessage("Are you sure you want to Exit?")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.colorPrimary)
                .addButton(
                        "Yes",
                        R.color.colorPrimary,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                finishAffinity();
                                pDialog.dismiss();
                            }
                        }
                )
                .addButton("No",
                        R.color.pdlg_color_red,
                        R.color.pdlg_color_white,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                pDialog.dismiss();
                            }
                        })
                .show();
    }

}
